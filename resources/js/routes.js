/**
 |-------------------------------------------------------------------------------
 | routes.js
 |-------------------------------------------------------------------------------
 | Contains all of the routes for the application
 */

/**
 Imports Vue and VueRouter to extend with the routes.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'

/**
 Extends Vue to use Vue Router
 */
Vue.use(VueRouter)

/**
 Makes a new VueRouter that we will use to run all of the routes
 for the app.
 */
export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: "/home",
            name: 'home',
            component: Vue.component('example-component', require('./components/ExampleComponent.vue').default),
        }
    ],

    /*
     * Scroll to top when navigating to a new route.
     */
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }
        else {
            return { x: 0, y: 0 };
        }
    },
});
